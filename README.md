# PerpustakaanApps REST API (MySQL menggunakan Flask)
Ini merupakan sebuah aplikasi _**Backend Microservice**_ sederhana untuk dipergunakan sebagai pengembangan aplikasi perpustakaan, terutama nantinya dari sisi Frontend. REST API ini menggabungkan microservice dari dua sisi database yang berbeda, yaitu mongodb sendiri dan juga mysql.
Pada REST API mysql untuk microservice ini, terdapat beberapa fungsionalitas REST API yang bisa digunakan terutama pada routing endpoint untuk customers dan borrows, seperti **melakukan request token**, **mendapatkan data seluruh user**, **mendapatkan data user berdasarkan id user**, **melakukan insert data user baru**, **melakukan update data user yang sudah ada berdasarkan id user**, **melakukan delete data user berdasarkan id user**, **mendapatkan data seluruh borrows atau peminjaman buku oleh user tertentu**, **melakukan insert untuk data borrows baru atau data peminjaman buku baru oleh user tertentu** dan terakhir **melakukan update borrows untuk status data peminjaman buku yang dilakukan oleh user sebelumnya (melakukan pengembalian buku)**. Microservice mysql ini akan dihubungkan dengan microservice mongodb untuk saling dipadukan antara Customers dan Borrows yang ada pada mysql dengan Books yang ada pada mongodb ini.

List lengkap untuk seluruh endpoint API yang ada pada MySQLApp ini:

1. Customers

    - **[GET]** http://localhost:5000/user/requesttoken

        `Params: email`
        
        Cara penggunaan: akses melalui _**Postman**_, lalu dilakukan pengaksesan pada url tersebut dengan penggunaan methods _GET_. Hasil akan didapatkan pada **Body** yaitu akses token untuk digunakan pada beberapa endpoint lainnya yang menggunakan JWT. Dengan rincian berisi key dan value yaitu:

        - data : value dictionary (username dan email)
        - token_access : value berupa string

        Pada endpoint ini, jangan lupa menambahkan **_params_** seperti yang dijelaskan sebelumnya pada Body di Postman. Contoh:
        
        `{
            "email": "ariwilyan@test.com"
        }`

    - **[GET]** http://localhost:5000/users
        
        Cara penggunaan: akses melalui _**Postman**_, lalu dilakukan pengaksesan pada url tersebut dengan penggunaan methods _GET_. Hasil akan didapatkan pada **Body** yaitu seluruh data user. Dengan rincian pada masing-masing data user tersebut, berisi key dan value yaitu:

        - id : value berupa integer
        - username : value berupa string
        - firstname : value berupa string
        - lastname : value berupa string
        - email : value berupa string

    - **[GET]** http://localhost:5000/user

        `Params: userid`
        
        Cara penggunaan: akses melalui _**Postman**_, lalu dilakukan pengaksesan pada url tersebut dengan penggunaan methods _GET_. Hasil akan didapatkan pada **Body** yaitu data user dengan id sesuai params yang dimasukkan. Dengan rincian pada data user tersebut, berisi key dan value yaitu:

        - id : value berupa integer
        - username : value berupa string
        - firstname : value berupa string
        - lastname : value berupa string
        - email : value berupa string

        Pada endpoint ini, jangan lupa menambahkan **_params_** seperti yang dijelaskan sebelumnya pada Body di Postman. Contoh:

        `{
            "userid": 6
        }`

        jangan lupa tambahkan juga pada **Header** yaitu _**Bearer Token**_ hasil yang didapatkan dari **generate access token** sebelumnya.

    - **[POST]** http://localhost:5000/user/insert

        `Params: values (dictionary)`
        
        Cara penggunaan: akses melalui _**Postman**_, lalu dilakukan pengaksesan pada url tersebut dengan penggunaan methods _POST_. Hasil akan didapatkan pada **Body** yaitu pesan/message apakah berhasil, tidak, atau error. Dengan rincian pada params values tersebut, berisi key dan value yaitu:

        - username : value berupa string
        - firstname : value berupa string
        - lastname : value berupa string
        - email : value berupa string

        Pada endpoint ini, jangan lupa menambahkan **_params_** seperti yang dijelaskan sebelumnya pada Body di Postman. Contoh:
        
        `{
            "values": {
                "username": "ariwilyan",
                "firstname": "ari",
                "lastname": "wilyan",
                "email": "ariwilyan@yahoo.com"
            }
        }`

        jangan lupa tambahkan juga pada **Header** yaitu _**Bearer Token**_ hasil yang didapatkan dari **generate access token** sebelumnya.

    - **[POST]** http://localhost:5000/user/update

        `Params: userid dan values`
        
        Cara penggunaan: akses melalui _**Postman**_, lalu dilakukan pengaksesan pada url tersebut dengan penggunaan methods _POST_. Hasil akan didapatkan pada **Body** yaitu pesan/message apakah berhasil, tidak, atau error. Dengan rincian pada params userid dan values tersebut, berisi key dan value yaitu:

        - userid : value berupa integer
        - username : value berupa string
        - firstname : value berupa string
        - lastname : value berupa string
        - email : value berupa string

        Pada endpoint ini, jangan lupa menambahkan **_params_** seperti yang dijelaskan sebelumnya pada Body di Postman. Contoh:
        
        `{
            "userid": 6,
            "values": {
                "username": "ariwilyanrd",
                "email": "ariwilyan@gmail.com"
            }
        }`

        jangan lupa tambahkan juga pada **Header** yaitu _**Bearer Token**_ hasil yang didapatkan dari **generate access token** sebelumnya.

    - **[POST]** http://localhost:5000/user/delete

        `Params: userid`
        
        Cara penggunaan: akses melalui _**Postman**_, lalu dilakukan pengaksesan pada url tersebut dengan penggunaan methods _POST_. Hasil akan didapatkan pada **Body** yaitu pesan/message apakah berhasil, tidak, atau error. Dengan rincian pada params userid tersebut, berisi value yaitu:

        - userid : value berupa integer

        Pada endpoint ini, jangan lupa menambahkan **_params_** seperti yang dijelaskan sebelumnya pada Body di Postman. Contoh:
        
        `{
            "userid": 6
        }`

        jangan lupa tambahkan juga pada **Header** yaitu _**Bearer Token**_ hasil yang didapatkan dari **generate access token** sebelumnya.

2. Borrows

    - **[GET]** http://localhost:5000/borrows

        `Params: email`
        
        Cara penggunaan: akses melalui _**Postman**_, lalu dilakukan pengaksesan pada url tersebut dengan penggunaan methods _GET_. Hasil akan didapatkan pada **Body** yaitu data borrows atau peminjaman yang dilakukan oleh user sesuai dengan sesuai dengan params dan token yang dimasukkan. Dengan rincian pada data borrows tersebut, berisi key dan value yaitu:

        - email : value berupa string
        - username : value berupa string
        - borrowid: value berupa integer
        - borrowdate: value berupa string
        - bookid: value berupa string
        - bookname: value berupa string
        - author: value berupa string
        - releaseyear: value berupa string
        - genre: value berupa string

        Pada endpoint ini, jangan lupa menambahkan **_params_** seperti yang dijelaskan sebelumnya pada Body di Postman. Contoh:
        
        `{
            "email": "ariwilyan@test.com"
        }`

        Contoh hasil pada body jika akses endpoint ini berhasil dan ada data:

        {
            "username": "ariwilyan",
            "borrowid": 2,
            "borrowdate": "Sun, 18 Apr 2021 02:42:24 GMT",
            "bookid": "606e84c97e67471003bcbf83",
            "bookname": "Harry Potter and the Goblet of Fire: The Illu",
            "author": "J. K. Rowling",
            "releaseyear": "2019",
            "genre": "Fiction"
        }

        jangan lupa tambahkan juga pada **Header** yaitu _**Bearer Token**_ hasil yang didapatkan dari **generate access token** sebelumnya.

    - **[POST]** http://localhost:5000/borrows/insert

        `Params: bookid`
        
        Cara penggunaan: akses melalui _**Postman**_, lalu dilakukan pengaksesan pada url tersebut dengan penggunaan methods _POST_. Hasil akan didapatkan pada **Body** yaitu pesan/message apakah berhasil, tidak, atau error. Dengan rincian pada params bookid tersebut, berisi value yaitu:

        - bookid : value berupa string

        Pada endpoint ini, jangan lupa menambahkan **_params_** seperti yang dijelaskan sebelumnya pada Body di Postman. Contoh:
        
        `{
            "bookid": "606e84c87e67471003bcbeeb"
        }`

        jangan lupa tambahkan juga pada **Header** yaitu _**Bearer Token**_ hasil yang didapatkan dari **generate access token** sebelumnya.

    - **[POST]** http://localhost:5000/borrows/status

        `Params: borrowid`
        
        Cara penggunaan: akses melalui _**Postman**_, lalu dilakukan pengaksesan pada url tersebut dengan penggunaan methods _POST_. Hasil akan didapatkan pada **Body** yaitu pesan/message apakah berhasil, tidak, atau error. Dengan rincian pada params borrowid tersebut, berisi value yaitu:

        - borrowid: value berupa integer

        Pada endpoint ini, jangan lupa menambahkan **_params_** seperti yang dijelaskan sebelumnya pada Body di Postman. Contoh:
        
        `{
            "borrowid": 1
        }`

        jangan lupa tambahkan juga pada **Header** yaitu _**Bearer Token**_ hasil yang didapatkan dari **generate access token** sebelumnya.

**ERD Microservice PerpustakaanApps (MySQL and MongoDB)**

![image](ERD.PNG)


**Note**:

> Untuk penggunaan REST API ini, jangan lupa menginstall dan menyiapkan seluruh library ataupun dependencies yang dibutuhkan, terutama Flask python.
Karena API ini dalam tahap testing, maka masih dijalankan pada **_server lokal_**.

Akses dengan mengaktifkan server mongodb pada FastAPI : **python main.py**

**NB**:

Terdapat pengembangan yaitu menambahkan **security berupa JWT** pada seluruh endpoint pada Customers, kecuali **_Get All Users_**
